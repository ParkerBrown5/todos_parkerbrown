package edu.towson.cosc431.brown.myapplication

import android.app.Activity
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import java.time.LocalDateTime


class NewToDoActivity : AppCompatActivity() {
    private lateinit var saveButton: Button
    private lateinit var titleTextBox: EditText
    private lateinit var contentsTextBox: EditText
    private lateinit var completedCheckBox: CheckBox
    private lateinit var dueDateText: EditText


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_to_do)
        grabWidgets()
        saveButton.setOnClickListener { saveTodo() }
    }
    fun grabWidgets(){
        saveButton=findViewById(R.id.SaveButton)
        titleTextBox=findViewById(R.id.TitleEditBox)
        contentsTextBox=findViewById(R.id.Contents)
        completedCheckBox=findViewById(R.id.completed)
        dueDateText=findViewById(R.id.DueDateEditText)
    }
    @RequiresApi(Build.VERSION_CODES.O)
    fun saveTodo(){
        val newTodo= ToDo(
            title= titleTextBox.editableText.toString(),
            contents= contentsTextBox.editableText.toString(),
            isCompleted = completedCheckBox.isChecked,
                dueDate = dueDateText.editableText.toString()

        )
        val intent= Intent()

        val gson= Gson()
        val json=gson.toJson(newTodo)
        intent.putExtra(TODO_KEY, json)
        setResult(RESULT_OK, intent)
        finish()

    }
    companion object{
        const val TODO_KEY="todo_key"
    }
}