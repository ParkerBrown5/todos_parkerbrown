package edu.towson.cosc431.brown.myapplication

data class ToDo (
    val title: String,
    val contents: String,
    val isCompleted: Boolean,

    val dueDate: String
)