package edu.towson.cosc435.labsapp

import android.content.Intent
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc431.brown.myapplication.*


class TodoAdapter(val TodoRepo: TodoRepository):RecyclerView.Adapter<TodoViewHolder> (){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.todo_view, parent, false)
        val holder= TodoViewHolder(view)


        return holder
    }

    override fun getItemCount(): Int {
        return TodoRepo.getCount()
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todo=TodoRepo.getTodo(position)
        holder.bindSong(todo)
    }

}
class TodoViewHolder(view: View):RecyclerView.ViewHolder(view){
    private val titleTextView= itemView.findViewById<TextView>(R.id.TitleTB)
    private val dueDateTextView= itemView.findViewById<TextView>(R.id.DueDateTBadd)
    private val isCompletedCheckBox=itemView.findViewById<CheckBox>(R.id.checkBox)
    private val contentsTextView=itemView.findViewById<TextView>(R.id.ContentsTB)
    private val editSong=itemView.findViewById<Button>(R.id.EditSongButton)
    val editButton=itemView.findViewById<Button>(R.id.EditSongButton)

    fun bindSong(todo:ToDo){
        titleTextView.text=todo.title
        dueDateTextView.text=todo.dueDate
        isCompletedCheckBox.isChecked=todo.isCompleted
        contentsTextView.text=todo.contents


    }


}