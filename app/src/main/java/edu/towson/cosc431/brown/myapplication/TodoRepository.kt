package edu.towson.cosc431.brown.myapplication

class TodoRepository {
    private val todos: MutableList<ToDo> = mutableListOf()
    init {
        addTodo(ToDo("Test1", "Contents1", true, "dueDate1"))
        addTodo(ToDo("Test2", "Contents2", true, "dueDate2"))
        addTodo(ToDo("Test3", "Contents3", true, "dueDate3"))
        addTodo(ToDo("Test4", "Contents4", true, "dueDate4"))
        addTodo(ToDo("Test5", "Contents5", true, "dueDate5"))
        addTodo(ToDo("Test6", "Contents6", true, "dueDate6"))
        addTodo(ToDo("Test7", "Contents7", true, "dueDate7"))
        addTodo(ToDo("Test8", "Contents8", true, "dueDate8"))
        addTodo(ToDo("Test9", "Contents9", true, "dueDate9"))
        addTodo(ToDo("Test10", "Contents10", true, "dueDate10"))
    }

    fun getTodo(idx: Int): ToDo {
        if(idx<0){
            return todos[0]
        }
        return todos[idx]

    }
    fun getCount():Int{
        return todos.size
    }
    fun getTodos():List<ToDo>{
        return todos
    }
    fun deleteTodo(idx:Int){
        todos.removeAt(idx)
    }
    fun addTodo(todo:ToDo){
        todos.add(todo)
    }



}