package edu.towson.cosc431.brown.myapplication

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc435.labsapp.TodoAdapter

class MainActivity : AppCompatActivity() {
    private lateinit var toDoButton: Button
    private lateinit var todoRepo: TodoRepository
    private lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        grabWidgetListners()
        todoRepo=TodoRepository()

        toDoButton.setOnClickListener { launchNewToDo() }
        recyclerView.adapter= TodoAdapter(todoRepo)
        recyclerView.layoutManager= LinearLayoutManager(this)

    }

    fun grabWidgetListners() {
        toDoButton = findViewById(R.id.addToDoButton)
        recyclerView=findViewById(R.id.recyclerView)
    }
    fun launchNewToDo(){
        val intent= Intent(this,NewToDoActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE)
    }
    companion object{
        const val REQUEST_CODE=1
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            REQUEST_CODE->{
                when(resultCode){
                    RESULT_OK->{
                        val json= data?.getStringExtra(NewToDoActivity.TODO_KEY)
                        if(json!=null){
                            Log.d("mainActivity", json)
                            val todo= Gson().fromJson<ToDo>(json, ToDo::class.java)
                            todoRepo.addTodo(todo)
                            recyclerView.adapter= TodoAdapter(todoRepo)
                            recyclerView.layoutManager= LinearLayoutManager(this)

                        }

                    }
                }
            }
        }
    }
}